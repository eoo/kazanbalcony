/*=======Modal Windows========*/

function ModalOpen(params) {
    var modal = params.modal;
    var btn = params.btn;
    var close = params.close;

    btn.onclick = function() {
        modal.style.display = "block";
        close.addEventListener("click", HandlerClose);
        modal.addEventListener("click", HandlerModalClose);
    }

    function HandlerClose() {
        modal.style.display = "none";
        close.removeEventListener("click", HandlerClose);
    }

    function HandlerModalClose() {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        modal.removeEventListener("click", HandlerModalClose);
    }


}
/*========button=======*/
$(function() {
    var btn = $('.btns li:first-child'),
        btn2 = $('.btns li:last-child');
    btn.click(function() {
        $(btn).removeClass('displayed');
        $(btn2).addClass('displayed');
    });
});

/*=============Slider===========*/

$(function() {
    var slider = $('.responses'),
        sliderContent = slider.html(),
        slideWidth = $('.wr-responses').outerWidth(),
        slideCount = $('.wr-responses li').length,
        prev = $('.wr-responses .prev'),
        next = $('.wr-responses .next'),
        markerLast = $('.markers li:last-child'),
        markerFirst = $('.markers li:first-child'),
        animateTime = 1000,
        course = 1,
        margin = -slideWidth;

    $('.wr-responses li:last').clone().prependTo('.wr-responses .responses');
    $('.wr-responses li').eq(1).clone().appendTo('.wr-responses .responses');
    $('.wr-responses .responses').css('margin-left', -slideWidth);

    function animate() {
        if (margin == -slideCount * slideWidth - slideWidth) {
            slider.css({ 'marginLeft': -slideWidth });
            margin = -slideWidth * 2;
        } else if (margin == 0 && course == -1) {
            slider.css({ 'marginLeft': -slideWidth * slideCount });
            margin = -slideWidth * slideCount + slideWidth;
        } else {
            margin = margin - slideWidth * (course);
        }
        slider.animate({ 'marginLeft': margin }, animateTime);
    }

    prev.click(function() {
        if (slider.is(':animated')) {
            return false;
        }
        var course2 = course;
        var marker = $('.markers li.active');
        course = -1;
        animate();
        course = course2;
        marker.removeClass('active');
        if (marker.prev().length == 0) {
            markerLast.addClass('active');
        } else {
            marker.prev().addClass('active');
        }
    });
    next.click(function() {
        if (slider.is(':animated')) {
            return false;
        }
        var course2 = course;
        var marker = $('.markers li.active');
        course = 1;
        animate();
        course = course2;
        marker.removeClass('active');
        if (marker.next().length == 0) {
            markerFirst.addClass('active');
        } else {
            marker.next().addClass('active');
        }
    });
});


$(function() {
    var slider = $('.gallery'),
        sliderContent = slider.html(),
        slideWidth = $('.wr-gallery').outerWidth(),
        slideCount = $('.wr-gallery li').length,
        prev = $('.wr-gallery .prev'),
        next = $('.wr-gallery .next'),
        animateTime = 1000,
        course = 1,
        margin = -slideWidth;

    $('.wr-gallery li:last').clone().prependTo('.wr-gallery .gallery');
    $('.wr-gallery li').eq(1).clone().appendTo('.wr-gallery .gallery');
    $('.wr-gallery .gallery').css('margin-left', -slideWidth);

    function animate() {
        if (margin == -slideCount * slideWidth - slideWidth) {
            slider.css({ 'marginLeft': -slideWidth });
            margin = -slideWidth * 2;
        } else if (margin == 0 && course == -1) {
            slider.css({ 'marginLeft': -slideWidth * slideCount });
            margin = -slideWidth * slideCount + slideWidth;
        } else {
            margin = margin - slideWidth * (course);
        }
        slider.animate({ 'marginLeft': margin }, animateTime);
    }

    prev.click(function() {
        if (slider.is(':animated')) {
            return false;
        }
        var course2 = course;
        course = -1;
        animate();
        course = course2;
    });
    next.click(function() {
        if (slider.is(':animated')) {
            return false;
        }
        var course2 = course;
        course = 1;
        animate();
        course = course2;
    });
});

/*=========Timer========*/
timeend = new Date(2017, 2, 5);

function time() {
    today = new Date();
    today = Math.floor((timeend - today) / 1000);
    tsec = today % 60;
    today = Math.floor(today / 60);
    if (tsec < 10) {
        tsec = '0' + tsec;
    }
    tmin = today % 60;
    today = Math.floor(today / 60);
    if (tmin < 10) {
    	tmin = '0' + tmin;
    }
    thour = today % 24;
    today = Math.floor(today / 24);
    document.getElementById('days').innerHTML = today;
    document.getElementById('hours').innerHTML = thour;
    document.getElementById('minutes').innerHTML = tmin;
    document.getElementById('seconds').innerHTML = tsec;
    window.setTimeout("time()", 1000);
}
/*=====scroll=====*/

(function(){
	$(document).ready(function() {
	    $('.nav').on('click', function(e) {
	    	if (e.target && e.target.nodeName === 'A') {
	    	    var item = $(e.target).attr('href');
	    	    $.scrollTo(item, 1500, {offset:-60});
	    	 }        
	    });
	});
})();
